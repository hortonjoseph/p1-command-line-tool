//
//  main.cpp
//  CommandLineTool
//
//  Created by Tom Mitchell on 08/09/2017.
//  Copyright © 2017 Tom Mitchell. All rights reserved.
//

#include <iostream>

int factorial (int userValue)
{
    int factorialCalc = 1;
    
    if (userValue > 0)
    {
        for (int i = 1; i <= userValue; i++) {
            factorialCalc *= i;
        }
    }
    return factorialCalc;
}

int main()
{
    // insert code here...
    int userInt;
    int factorialReturn;
    
    std::cout << "Enter a whole number and have its factorial calculated: ";
    std::cin >> userInt;
    factorialReturn = factorial(userInt);
    std::cout << userInt << "! = " << factorialReturn << "\n";
}


